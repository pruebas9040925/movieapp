export function mapGenres(genIds, genres) {
    if (!Array.isArray(genres)) {
        return "Genres data is not valid";
    }

    let genresMap = genres.reduce((result, current) => {
        result[current.id] = current.name;
        return result;
    }, {});
    
    return genIds.map((id) => genresMap[id]).join(", ");
}