import React from 'react'
import {ImageList,ImageListItem,ImageListItemBar,useMediaQuery} from '@mui/material';
import { Link } from 'react-router-dom';
import { IMAGES_PATH } from '../config';
import { mapGenres } from '../helpers';
import {styled} from '@mui/system'
import { useTheme } from '@mui/material/styles';


export const Movies = ({movies,genres}) => {

  const theme = useTheme();
  const matchDownMd= useMediaQuery(theme.breakpoints.down('sm'));

  const ImgStyled = styled('img')({
    width:'100%',
    height:'100%',
    objectFit:'cover'
  });

  const ImageListItemStyled = styled(ImageListItem)({
    overflow:'hidden'
  });

  return (
    <ImageList cols={matchDownMd? 1:5} rowHeight={365} gap={12}>
        {
            movies.results.map((movie,i)=>(
              <ImageListItemStyled key={i}>
                <Link to={`/movie/${movie.id}`}>
                  {movie.poster_path &&(
                    <ImgStyled src={`${IMAGES_PATH}/w300${movie.poster_path}`} alt={movie.title} />
                  )}
                  <ImageListItemBar title={movie.title} subtitle={
                  <span>
                    {mapGenres(movie.genre_ids,genres)}
                  </span>}/>
                </Link>
              </ImageListItemStyled>
            ))
        
        }
    </ImageList>
  )
}
export default Movies;
