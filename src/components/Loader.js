import React from 'react';
import {CircularProgress} from '@mui/material';
import {styled} from '@mui/system';

export const Loader = () => {

    const LoaderWrapper = styled('div')(({theme})=>({
        display:'flex',
        justifyContent:'center',
        marginTop:theme.spacing(3)
    }))

  return (
    <div>
        <LoaderWrapper>
            <CircularProgress/> 
        </LoaderWrapper>
        
    </div>
  )
}

export default Loader;