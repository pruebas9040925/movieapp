import React,{useEffect} from 'react';
import {useDispatch,useSelector} from 'react-redux';
import {useParams} from 'react-router-dom';
import movie,{ getMovie,resetState } from '../redux/movie/reducers';
import Loader from '../components/Loader';
import { Movie } from '../components/Movie';

const MovieDetails = () => {
  const {movie} = useSelector((store)=>store);
  const {genres} = useSelector((store)=>store.genres)

  const dispatch = useDispatch();
  const {id} = useParams();

  useEffect(()=>{
    dispatch(getMovie(id?parseInt(id,10):0));

    return ()=>{
      dispatch(resetState());
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[dispatch]);

  useEffect(()=>{
    if(id !== movie.id?.toString())
    dispatch(getMovie(id?parseInt(id,10):0));
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[id, movie.id])

  return (movie.isFetching ? <Loader/>:<Movie movie={movie} genres={genres}/>)
}

export default MovieDetails