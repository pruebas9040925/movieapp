import {delay,call,put, takeEvery} from 'redux-saga/effects';
import {fetchedMovie,getMovie } from './reducers';
import { API_KEY } from '../../config';
import TheMovieDbApi from '../../services';


const api = new TheMovieDbApi(API_KEY);

function* fetchMovie(action){
    yield delay(500);
    let resp = yield call(api.getMovie,action.payload);

    yield put(
        fetchedMovie(resp)
    )
}


export function* watchMovie(){
    yield takeEvery(getMovie.type,fetchMovie)
}
