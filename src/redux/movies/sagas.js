import {delay,call,put, takeEvery} from 'redux-saga/effects';
import { getPopularMovies,fetchedPopularMovies } from './reducers';
import { API_KEY } from '../../config';
import TheMovieDbApi from '../../services';


const api = new TheMovieDbApi(API_KEY);

function* fetchPopularMovies(action){
    yield delay(500);
    let resp = yield call(api.getPopularMovies,action.payload);

    yield put(
        fetchedPopularMovies(resp)
    )
}


export function* watchPopularMovies(){
    yield takeEvery(getPopularMovies.type,fetchPopularMovies)
}
