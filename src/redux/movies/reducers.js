import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  results: [],
  hasMore:false,
  totalResults:0,
  page:0,
  totalPage:0,
  isFetching: false,
};

const MoviesSlice = createSlice({
  name: "moviesSlice",
  initialState,
  reducers: {
    getPopularMovies: (state) => {
      return {
        ...state,
        isFetching: true,
      };
    },
    fetchedPopularMovies: (state,action) => {
      return {
        ...state,
        results:[...state.results,...action.payload.results],
        hasMore:action.payload.page < action.payload.total_pages,
        totalResults:action.payload.total_results,
        page:action.payload.page,
        totalPage:action.payload.totalPage,
        isFetching: false,
      };
    },
    resetState: (state) => {
      return initialState;
    },

  },
});

export const { getPopularMovies,fetchedPopularMovies,resetState} = MoviesSlice.actions;
export default MoviesSlice.reducer;
