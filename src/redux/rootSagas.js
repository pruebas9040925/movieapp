import {all,fork} from 'redux-saga/effects';
import { watchSearchMovies } from './search/sagas';  
import { watchGenres } from './genre/sagas';
import { watchPopularMovies } from './movies/sagas';
import { watchMovie } from './movie/sagas';

export default function* watcherSaga(){
    yield all([
        fork(watchSearchMovies),
        fork(watchGenres),
        fork(watchPopularMovies),
        fork(watchMovie)
    ])

}
