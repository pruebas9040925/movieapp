import { configureStore } from "@reduxjs/toolkit";
import searchReducer from './search/reducers';
import watcherSaga from "./rootSagas";
import createSagaMiddleware from '@redux-saga/core';
import genresReducer from './genre/reducers';
import MoviesReducer from './movies/reducers';
import MovieReducer from './movie/reducers';

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
    reducer:{
        search: searchReducer,
        genres:genresReducer,
        movies:MoviesReducer,
        movie:MovieReducer
    },
    
    middleware: [
        sagaMiddleware
    ],
});

sagaMiddleware.run(watcherSaga);

export default store;