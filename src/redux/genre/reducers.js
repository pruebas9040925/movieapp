import { createSlice } from "@reduxjs/toolkit";
import {GENRE_REDUCER, GET_GENRES,FETCHED_GENRES,RESET_STATE_GENRES } from './types'

const initialState = {
  genres: [],
  isFetching: false,
};

const genreSlice = createSlice({
  name: GENRE_REDUCER,
  initialState,
  reducers: {
    getGenres: (state) => {
      return {
        ...state,
        isFetching: true,
      };
    },
    fetchedGenres: (state,action) => {
      return {
        ...state,
        genres:action.payload.genres,
        isFetching: false,
      };
    },
    resetState: (state) => {
      return initialState;
    },

  },
});

export const { getGenres,fetchedGenres,resetState} = genreSlice.actions;
export default genreSlice.reducer;
