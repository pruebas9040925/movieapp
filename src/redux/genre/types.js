export const GET_GENRES = 'genre/GET_GENRES';
export const FETCHED_GENRES = 'genre/FETCHED_GENRES'
export const RESET_STATE_GENRES= 'genre/RESET_STATE_GENRES';
export const GENRE_REDUCER = 'genre/GENRE_REDUCER';