import {delay,call,put, takeEvery} from 'redux-saga/effects';
import { getGenres,fetchedGenres } from './reducers';
import { API_KEY } from '../../config';
import TheMovieDbApi from '../../services';


const api = new TheMovieDbApi(API_KEY);

function* fetchGenres(action){
    yield delay(500);
    let resp = yield call(api.getGenres,action.payload);

    yield put(
        fetchedGenres(resp)
    )
}


export function* watchGenres(){
    yield takeEvery(getGenres.type,fetchGenres)
}
