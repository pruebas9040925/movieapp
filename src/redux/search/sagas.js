import {delay,call,put,takeLatest} from 'redux-saga/effects';
import { searchMovies,fetchedSearchMovies } from './reducers';
import { API_KEY } from '../../config';
import TheMovieDbApi from '../../services';


const api = new TheMovieDbApi(API_KEY);

function* fetchSearchMovies(action){
    yield delay(500);
    let resp = yield call(api.searchMovies,action.payload);
    yield put(
        fetchedSearchMovies(resp)
    )
}


export function* watchSearchMovies(){
    yield takeLatest(searchMovies.type,fetchSearchMovies)
}
