import { createSlice } from "@reduxjs/toolkit";
// Action Types
// const SEARCH_MOVIES = "SEARCH_MOVIES";


const initialState = {
    results: [],
    totalResults:0,
    page:0,
    totalPages:0,
    isFetching:false
}

const searchSlice = createSlice({
    name:'searchSlice',
    initialState,
    reducers:{
        searchMovies:(state)=>{
            return {
                // type:SEARCH_MOVIES,
                ...state,
                // payload:{state},
                isFetching:true
            };
        },
        fetchedSearchMovies:(state,action)=>{
            return {
                ...state,
                isFetching:false,
                results:action.payload.results,
                totalResults:action.payload.total_results,
                page:action.payload.page,
                totalPages:action.payload.total_pages
            };
        },
        resetState:(state)=>{
            return initialState
        }
    }
});

export const {searchMovies,fetchedSearchMovies,resetState } = searchSlice.actions;
export default searchSlice.reducer;